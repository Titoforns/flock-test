import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { HttpClientModule } from '@angular/common/http';
import { PlacesService } from 'src/app/services/places.service';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';

import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import { SearchComponent } from 'src/app/components/search/search.component';
import { mapSafeUrlPipe } from '../../pipes/mapsafeurl.pipe';
import {MatToolbarModule} from '@angular/material/toolbar';
import { ToolbarComponent } from 'src/app/components/toolbar/toolbar.component';

@NgModule({
  declarations: [HomeComponent, SearchComponent, mapSafeUrlPipe, ToolbarComponent],
  imports: [
    CommonModule,
    CommonModule,
    MatIconModule,
    FlexLayoutModule,
    MatInputModule,
    ReactiveFormsModule,
    MatButtonModule,
    HomeRoutingModule,
    FlexLayoutModule,
    FormsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatCardModule,
    MatToolbarModule
  ],
  providers: [
    PlacesService
  ]
})
export class HomeModule {
}
