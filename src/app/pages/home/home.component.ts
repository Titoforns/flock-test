import { Component, OnInit } from '@angular/core';
import { PlacesService } from 'src/app/services/places.service';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { Place } from 'src/app/classes/place';
import { User } from 'src/app/classes/user';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  provinceName: string = '';

  myControl = new FormControl();
  options: Place[] = [];
  filteredOptions: Observable<Place[]> = new Observable<Place[]>();
  matches: number = 0;
  searched = false;
  filteredProvinces: Place[] = [];

  constructor(private placesService: PlacesService) { 
    this.setInitialData();
  }

  ngOnInit(): void {
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => (typeof value === 'string' ? value : value.nombre)),
      map(nombre => (nombre ? this._filter(nombre) : this.options.slice())),
    );
    console.error(this.filteredOptions);
  }

  getData() {
    return this.placesService.getPlacesByName(this.provinceName);
  }

  search() { 
    this.provinceName = this.myControl.value ? (this.myControl.value.nombre ? this.myControl.value.nombre : this.myControl.value) : '';
    this.getData().then((d: any) => {
      this.filteredProvinces = d.provincias;
      this.matches = d.cantidad;
    }).catch(e => {
      console.error(e);
    })
    this.searched = true;
  }

  setInitialData() {
    this.getData().then(d => {
      this.setData(d);
    }).catch(e => {
      console.error(e);
    })
  }

  setData(d: any) {
    this.options = d.provincias;
    this.filteredProvinces = d.provincias;
    this.matches = d.cantidad;
  }

  displayFn(place: Place): string {
    return place && place.nombre ? place.nombre : '';
  }

  private _filter(name: string): Place[] {
    const filterValue = name.toLowerCase();

    return this.options.filter(option => option.nombre.toLowerCase().includes(filterValue));
  }

}
