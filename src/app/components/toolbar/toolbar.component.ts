import { Component, Input, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

 

  constructor(private authService: AuthenticationService) { 
    
  }

  ngOnInit(): void {
    
  }

  logout() {
    this.authService.logout();
  }

}


