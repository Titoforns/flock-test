import { Component, Input, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { Place } from 'src/app/classes/place';
import { Loader, LoaderOptions } from 'google-maps';
import { PlacesService } from 'src/app/services/places.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  @Input() place: Place = new Place();

  googleMapUrl = 'https://www.google.com.ar/maps/dir/';

  options: LoaderOptions = {};
  loader = new Loader('AIzaSyBGv-LNZ5-OmrHeXKHRVbelojiSRQXO4O4', this.options);
  
  google: any;
  map: any;
  municipiosLength = 0;
  mapUrl: any = null;

  constructor(private placesService: PlacesService) { 
    
  }

  ngOnInit(): void {
    this.mapUrl = 'https://www.google.com/maps/embed/v1/place?q=' + this.place.centroide.lat + ',' + this.place.centroide.lon + '&amp;key=AIzaSyBGv-LNZ5-OmrHeXKHRVbelojiSRQXO4O4'
    //this.loadData();
  }

  async initMap() {
    this.google = await this.loader.load();
    let htmlEl: any = document.getElementById('map');
    this.map = new google.maps.Map(htmlEl, {
        center: {lat: -34.397, lng: 150.644}
    });
  }

  async loadData() {
    this.placesService.getTownsById(this.place.id).then((info: any) => {
      console.warn(info);
      this.municipiosLength = info.total ? info.total.length : 0;
    })
  }

}


