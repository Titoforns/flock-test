import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class AuthenticationService {

    constructor(private router: Router) {
        
    }
    
    logout() {
        localStorage.removeItem('testUserLogged');
        this.router.navigate(['login']);
    }


    login(name: string) {
        let usr = {
            name: name,
            logged: true
        }
        localStorage.setItem('testUserLogged', JSON.stringify(usr));
        this.router.navigate(['home']);
    }

    getLogged(): boolean {
        if(localStorage.getItem('testUserLogged') && JSON.parse(<any>localStorage.getItem('testUserLogged')).logged) {
            return true;
        } else {
            this.router.navigate(['login']);
            return false;
        }
    }

}
