import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class PlacesService {

    baseUrl = 'https://apis.datos.gob.ar/georef/api/provincias';
    townsUrl = 'https://apis.datos.gob.ar/georef/api/municipios?provincia='

    constructor(private http: HttpClient) {
        
    }


    getPlacesByName(name: string) {
        let url = this.baseUrl + (name ? '?nombre=' + name : '');
        return new Promise( (resolve, reject) => {
            this.http.get(url).subscribe( r => {
                resolve(r);
            }, e => {
                console.error(e)
                reject(e);
            });
        });
    }


    getTownsById(id: string) {
        let url = this.townsUrl + id + '&max=1000';
        return new Promise( (resolve, reject) => {
            this.http.get(url).subscribe( r => {
                resolve(r);
            }, e => {
                console.error(e)
                reject(e);
            });
        });
    }


}
