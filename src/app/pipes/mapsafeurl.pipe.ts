import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'mapSafeUrlPipe'
})
export class mapSafeUrlPipe implements PipeTransform {

  constructor ( private _domSanitizer:DomSanitizer){}

  transform( c: any ): any {

    return this._domSanitizer.bypassSecurityTrustResourceUrl( 'https://www.google.com/maps/embed/v1/place?q=' + c.lat + ',' + c.lon + '&key=AIzaSyBGv-LNZ5-OmrHeXKHRVbelojiSRQXO4O4' );

  }

}