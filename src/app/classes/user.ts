export class User {
    name: string;
    lastName: string;
    userName: string;
    id: string;
    userPhoto: string;
    state: boolean;

    constructor() {
        this.name = '';
        this.lastName = '';
        this.userName = '';
        this.id = '';
        this.userPhoto = '';
        this.state = false;
    }
}
