export class Place {
    nombre: string;
    centroide: Centroide;
    id: string;

    constructor(nombre?: string, id?: string, lat?: number, lon?: number) {
        this.nombre = nombre || '';
        this.centroide = new Centroide(lat || 0, lon || 0);
        this.id = id || '';
    }
}

class Centroide {
    lat: number;
    lon: number;

    constructor(lat: number, lon: number) {
        this.lat = lat || 0;
        this.lon = lon || 0;
    }
}
